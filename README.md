# arg_dot_h

Argument parser somewhat following the style seen in many C applications:

* [OpenBSD ls](https://github.com/openbsd/src/blob/master/bin/ls/ls.c#L135)
* [GNU ls](https://github.com/coreutils/coreutils/blob/master/src/ls.c#L1966)
* [Plan9 ls](https://github.com/brho/plan9/blob/master/sys/src/cmd/ls.c#L65)

Example:

```rust
use std::process;

use arg_dot_h::*;

fn help(argv0: &str) {
    println!("{} [-b] [-s string]", argv0);
    process::exit(1);
}

fn main() {
    let mut argv0 = String::new();
    let mut testbool = false;
    let mut teststring = String::new();

    // Parses std::env::args(). If an argument starts with '-', it will remove the '-' and iterate
    // over the chars. After everything is parsed, anything that didn't start with '-' or wasn't taken with eargf is returned in a Vec<String>
    let args = argbegin! {
        // First argument is the variable the name of the program will be set to (since it can be renamed)
        &mut argv0,
        // All other arguments are like a match statement for each char in a valid argument.
        // Run when the char is 'b'
        'b' => testbool = true,
        // eargf! returns the next argument. If a function (help(&argv0)) is passed, it will run
        // that if there isn't a next argument. Otherwise it will return an empty string.
        's' => teststring = eargf!(help(&argv0)),
        // If the char doesn't match anything else, run the help function.
        _ => help(&argv0)
    };

    /* If "-bs mystring more args" was passed, it would output:
    true mystring
    more
    args
    */
    println!("{} {}", testbool, teststring);
    args.iter().for_each(|arg| println!("{}", arg));
}
```
